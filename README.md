# Model checking IKEv2 using Spin

This repository contains code to analyze the IKEv2 protocol using the Spin model
checker.

Full details are available in:
-   [the Ph.D. thesis](http://www.theses.fr/en/2020REN1S002), Sections 3.2 and 3.3
-   [the conference paper](https://ieeexplore.ieee.org/document/8949057)

The `slides/` directory contains the slides of our conference presentation.
